#ifndef STACK_CLASS_HPP
#define STACK_CLASS_HPP

// Necessary includes
/*
 * NAME:        Sri Padala
 * Description: A Templated Stack Class Implementation. 
*/

template <typename T>
class Stack {
public:
    // default constructor
    Stack();
    // parameterized constructor that takes first item
    Stack(T firstelem);
    // copy constructor
    Stack(const Stack<T>& item);
    // move constructor
    Stack(Stack<T>&& item);
    // destructor
    ~Stack();
    // function empty; does not throw exceptions
    bool empty()const noexcept{return(size() == 0);}
    // function size; does not throw exceptions
    unsigned int size() const noexcept{return(count);}
    // function top; l-value; throws underflow if stack is empty
    T& top();
    // function top; read-only; throws underflow if stack is empty
    const T& top() const;
    // function push; does not throw exceptions
    void push(const T& item);
    // function emplace; does not throw exceptions
    template <typename... Args>void emplace(Args&&... args);
    // function pop; throws underflow if stack is empty
    void pop();
    // copy assignment operator overload
    Stack<T>& operator = (const Stack<T>& item);
    // move assignment operator overload
    Stack<T>& operator = (Stack<T>&& item);
private:
    // Private data
    class Node;
    Node* top_pointer;
    unsigned int count;
};

template <typename T>
class Stack<T>::Node {
public:
    // Functions that you deem necessary
    Node(){}
    Node(T thedata,Node* pointer)
        :data(thedata),next_pointer(pointer){}
    Node* getnextLink() const{return next_pointer;}
    T& getData(){return data;}
    const T& getData()const{return data;}
    void setData(T theData){data = theData;}
    void settopLink(Node* pointer){next_pointer = pointer;}
private:
    T data;
    Node* next_pointer;
};

// STACK FUNCTIONS
template<class T>
Stack<T>::Stack()//Default Constructor
    :top_pointer(nullptr),count(0){}

template<class T>
Stack<T>::Stack(T firstelem){
    top_pointer = new Node(firstelem,nullptr);
    count = 1;
}
template<class T>
Stack<T>::Stack(const Stack<T>& item){
    top_pointer = item.top_pointer;//common ownership
    count = item.count;
}
template<class T>
Stack<T>::Stack(Stack<T>&& item):top_pointer(item.top_pointer),count(item.count){
    item.top_pointer = nullptr;//Tanfering ownership
    item.count = 0;
}
template<class T>
Stack<T>::~Stack(){}

template<class T>
T& Stack<T>::top(){
    if(top_pointer == nullptr){
        throw std::underflow_error("stack is empty");//throws the exception
    }else{
        return(top_pointer->getData());
    }
}

template<class T>
const T& Stack<T>::top() const{
    if(top_pointer == nullptr){
        throw std::underflow_error("stack is empty");//throws the exception
    }else{
        return(top_pointer->getData());
    }
}

template<class T>
void Stack<T>::push(const T& item){
    Node* temp = new Node(item,nullptr);
    if(top_pointer == nullptr){//if empty make the new node a stack
        top_pointer = temp;
    }else{
        temp->settopLink(top_pointer);
        top_pointer = temp;
    }
    count++;
}

template<class T>
template <typename... Args>
void Stack<T>::emplace(Args&&... args){
    push(std::move(T(std::forward<Args>(args) ...)));
}
template<class T>
void Stack<T>::pop(){
    if(top_pointer == nullptr){
        throw std::underflow_error("stack is empty");
    }else{
        Node* temp = top_pointer;
        top_pointer = top_pointer->getnextLink();
        delete temp;
        temp = nullptr;
        count--;
    }
}

template<class T>
Stack<T>& Stack<T>::operator = (const Stack<T>& item){

    if(top_pointer != item.top_pointer){//common ownership
        top_pointer = item.top_pointer;
        count = item.count;
    }
    return(*this);

}
template<class T>
Stack<T>& Stack<T>::operator = (Stack<T>&& item){
    if(top_pointer != item.top_pointer){
        top_pointer = item.top_pointer;
        count = item.count;
        item.top_pointer = nullptr;//tranfering ownership
        item.count = 0;
    }
    return(*this);
}
#endif